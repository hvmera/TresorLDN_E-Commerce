
<!doctype html>
<html>
        <head>
<title>About us page </title> 
<style>
.section {
  background-color: beige;
  color: grey;
  border: 2px solid black;
  margin: 20px;
  padding: 20px;
}

#myHeader {
position:center;       

}

</style>
</head>
<body>
        <h1 id="myHeader"> About Us</h1>
        <div class="section">
        <h2>Our story</h2> 
<p> Our brand is called TresorLdn. 
    Tresor stands for treasure, we believed this is fitting as the furniture
    you purchase from us will be treasured by yourselves for a very long time 
    and we are based in London. Shop TresorLdn from the comfort of your own home.
     You can browse and shop many products on our website, and have them delivered 
     straight to your door.
</p>

</div>
 <div class="section">       
<h2> Sustainability </h2>

<p2> We wish to encourage our customers to live more sustainably, so we're 
     concentrating on affordable, resource and energy efficient products.  
     With these little changes and energy-saving solutions we are paving 
     the way for more sustainable homes. We are continuing our journey
     towards only sourcing renewable or recycled materials by 2030. 
     We aim to use only renewable or recycled materials and to provide
     new solutions for our customers to prolong the life of products and materials.  
</p2>  
</div>     

<div class= "section">
<h2> CEO's </h2>
<p> We had the idea during a team project in University where we were told to create 
        a website selling products. Shortly after, we decided to bring the idea to life. 
        The CEO's of the company are: 
        <li>Humera Muhammad</li> 
        <li>Ibrahim Ahmad</li> 
        <li>Ben Squelch</li>
        <li>Thomas Merricks</li> 
        <li> Anaami Patel</li> 
        <li>Zainab Desai</li> 
        <li>Sachin Kumar</li> 
        <li>Boluwatife Akinola</li>
</div>
</body>
</html>